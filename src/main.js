import Jeu from './components/Jeu.js';

let listJeu = new Array();

fetch(
	'https://api.rawg.io/api/games?dates=2020-01-01,3000-01-01&metacritic=50,100'
)
	.then(response => response.json())
	.then(data => {
		console.log(data.results);
		data.results.forEach(element => {
			listJeu.push(new Jeu(element));
		});
		console.log(listJeu);
	});
