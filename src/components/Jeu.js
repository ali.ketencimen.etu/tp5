import Component from './Component.js';
import Img from './Img.js';

export default class Jeu extends Component {
	constructor({
		image,
		titre,
		description,
		note,
		plateformes,
		genres,
		vignettes,
	}) {
		super('article', { name: 'class', value: 'Jeu' }, [
			new Component('a', { name: 'href', value: image }, [
				new Img(image),
				new Component('section', null, [
					new Component('h3', null, titre),
					new Component('h4', null, description),
					new Component('h4', null, note),
					new Component('h4', null, plateformes),
					new Component('h4', null, genres),
					new Component(
						'a',
						{ name: 'href', value: vignettes },
						new Img(vignettes)
					),
				]),
			]),
		]);
	}

	render() {
		return `<img src="${this.image}">
                    <h5> ${this.titre} </h5><h5> ${this.note} </h5>
                    <button type=submit>Ajouter aux favoris</button>`;
	}
}
